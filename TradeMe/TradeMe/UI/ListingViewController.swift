//
//  ViewController.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import UIKit

class ListingViewController: UIViewController, UIViewControllerTransitioningDelegate, Loadable {
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentCategory: TradeMeCategory? {
        didSet {
            if let categoryNumber = currentCategory?.Number {
                title = currentCategory?.Name
                let getSearchItemsRequest = GetSearchItemsRequest(categoryNumber: categoryNumber, delegate: self)
                getSearchItemsRequest.executeRequest(withResponseType: GetSearchItemsResponse.self)
            } else {
                print("Category: \(String(describing: currentCategory?.Name ?? "" )) doesn't have Number, so no item is displayed")
                currentListing = []
            }
        }
    }
    
    var currentListing: [ListingItem] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    let categoryContainer = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoryContainer") as! UINavigationController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = ""
        
        tableView.tableFooterView = UIView()
        
        searchController.searchResultsUpdater = self
//        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Candies"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        
        showLoadingIndicator()
        
        let request = GetCategoriesRequest(delegate: self)
        request.executeRequest(withResponseType: TradeMeCategory.self)
        
        let getSearchItemsRequest = GetSearchItemsRequest(delegate: self)
        getSearchItemsRequest.executeRequest(withResponseType: GetSearchItemsResponse.self)
        
        categoryContainer.transitioningDelegate = self
        categoryContainer.modalPresentationStyle = .custom
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return BottomPresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    @IBAction func showCategories(_ sender: Any) {
        present(categoryContainer, animated: true, completion: nil)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "ShowListingItemDetail" {
            if let selectedRow = tableView.indexPathForSelectedRow?.row, selectedRow >= 0 {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowListingItemDetail" {
            let listingItemDetailViewController = segue.destination as! ListingItemDetailViewController
            listingItemDetailViewController.listingItem = currentListing[tableView.indexPathForSelectedRow!.row]
        }
    }
}

extension ListingViewController: CategorySelectionDelegate {
    func didSelectCategory(category: TradeMeCategory) {
        // first time
        if currentCategory == nil,
           let selectedCategoryNumber = category.Number,
           selectedCategoryNumber.isEmpty {
                return
        }
        
        if currentCategory?.Number == category.Number {
            return
        }
        
        currentCategory = category
    }
}

extension ListingViewController: GetSearchItemsRequestDelegate {
    func handleGetSearchItemsSuccessWithObject<T>(response: SuccessResponse<T>) {
        currentListing = (response.data as? GetSearchItemsResponse)?.List ?? []
        
        hideLoadingIndicator()
    }
    
    func handleGetSearchItemsFailure() {
        print("handleGetSearchItemsFailure")
        
        hideLoadingIndicator()
    }
}

extension ListingViewController: UITableViewDelegate {
    
}

extension ListingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentListing.count > Constants.maxListing ? Constants.maxListing : currentListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
        cell.textLabel?.text = currentListing[indexPath.row].Title
        return cell
    }
}

extension ListingViewController: GetCategoriesRequestDelegate {
    func handleGetCategoriesSuccessWithObject<T>(response: SuccessResponse<T>) {
        let rootCategory = response.data as? TradeMeCategory
        let rootCategoryListViewController = categoryContainer.viewControllers[0] as! CategoryListViewController
        rootCategoryListViewController.category = rootCategory
        rootCategoryListViewController.delegate = self
    }
    
    func handleGetCategoriesFailure() {
        print("handleGetCategoriesFailure")
    }
}

extension ListingViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
    
    }
}

extension ListingViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let getSearchItemsRequest = GetSearchItemsRequest(categoryNumber: currentCategory?.Number ,searchString: searchBar.text, delegate: self)
        getSearchItemsRequest.executeRequest(withResponseType: GetSearchItemsResponse.self)
    }
}

