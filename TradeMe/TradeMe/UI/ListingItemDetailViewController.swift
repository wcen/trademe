//
//  ListingItemDetailViewController.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation
import UIKit
import ImageSlideshow

class ListingItemDetailViewController: UIViewController {
    var listingItem: ListingItem!
    @IBOutlet weak var imageSlideShow: ImageSlideshow!
    @IBOutlet weak var listingItemTitle: UILabel!
    @IBOutlet weak var listingItemPrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = listingItem.Title
        
        let getSearchItemsRequest = GetListingItemRequest(listingId: listingItem.ListingId, delegate: self)
        getSearchItemsRequest.executeRequest(withResponseType: ListingItem.self)
        
        imageSlideShow.backgroundColor = .black
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.lightGray
        pageIndicator.pageIndicatorTintColor = UIColor.black
        imageSlideShow.pageIndicator = pageIndicator
    }
}

extension ListingItemDetailViewController: GetListingItemRequestDelegate {
    func handleGetListingItemSuccessWithObject<T>(response: SuccessResponse<T>) {
        if let listingItem = response.data as? ListingItem {
            print(listingItem)
            self.listingItem = listingItem
            
            listingItemTitle.text = listingItem.Title
            listingItemPrice.text = listingItem.PriceDisplay
            var imageUrls = [URL]()
            
            if let photos = listingItem.Photos {
                for photo in photos {
                    if let galleryUrl =  photo.Value?.Gallery,
                       let url = URL(string: galleryUrl) {
                        imageUrls.append(url)
                    }
                }
            }
            let imageSource = imageUrls.map { KingfisherSource.init(url: $0) }
            imageSlideShow.setImageInputs(imageSource)
        }
    }
    
    func handleGetListingItemFailure() {
        
    }
}
