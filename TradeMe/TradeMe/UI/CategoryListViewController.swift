//
//  CategoryListViewController.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation
import UIKit

protocol CategorySelectionDelegate: class {
    func didSelectCategory(category: TradeMeCategory)
}

class CategoryListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: CategorySelectionDelegate?
    
    var category: TradeMeCategory!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        title = category.Name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegate?.didSelectCategory(category: category)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "ShowSubCategories" {
            if let selectedSubCategory = category?.Subcategories?[tableView.indexPathForSelectedRow!.row] {
                if let subCategories = selectedSubCategory.Subcategories,
                   subCategories.count > 0 {
                    return true
                }
                delegate?.didSelectCategory(category: selectedSubCategory)
            }
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSubCategories" {
            let controller = segue.destination as! CategoryListViewController
            controller.category = category?.Subcategories?[tableView.indexPathForSelectedRow!.row]
            controller.delegate = delegate
            
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
    }
}

extension CategoryListViewController: UITableViewDelegate {}

extension CategoryListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category?.Subcategories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        cell.textLabel?.text = category?.Subcategories?[indexPath.row].Name
        
        if let subCategories = category?.Subcategories?[indexPath.row].Subcategories,
              subCategories.count > 0 {
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
}
