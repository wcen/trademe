//
//  UIViewController+Loadable.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import UIKit

protocol Loadable {
    func showLoadingIndicator()
    func hideLoadingIndicator()
}

extension Loadable where Self:UIViewController {
    func showLoadingIndicator() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = false
            let spinnerView = UIView.init(frame: self.view.bounds)
            spinnerView.tag = 10000
            spinnerView.backgroundColor = UIColor.clear
            let ai = UIActivityIndicatorView.init()
            ai.color = .black
            ai.startAnimating()
            ai.center = spinnerView.center
            spinnerView.addSubview(ai)
            self.view.addSubview(spinnerView)
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = true
            self.view.viewWithTag(10000)?.removeFromSuperview()
        }
    }
}
