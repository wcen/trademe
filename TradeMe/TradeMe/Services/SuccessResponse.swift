//
//  SuccessResponse.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

public struct SuccessResponse<T> {
    public var data: T
    public var response: URLResponse
}
