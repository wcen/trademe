//
//  GetSearchItemsResponse.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

struct GetSearchItemsResponse: Codable {
    let List: [ListingItem]?
}
