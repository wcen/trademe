//
//  ListingItem.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

struct ListingItem: Codable {
    let ListingId: Int
    let Title: String?
    let Region: String?
    let PriceDisplay:  String?
    let Photos: [Photo]?
}

struct Photo: Codable{
    let Value: PhotoUrl?
}

struct PhotoUrl: Codable{
    let Gallery: String?
}
