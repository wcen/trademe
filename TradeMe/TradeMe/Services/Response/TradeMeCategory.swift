//
//  GetCategoriesResponse.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

struct TradeMeCategory: Codable {
    let Name: String?
    let Number: String?
    let Path: String?
    let Subcategories: [TradeMeCategory]?
}
