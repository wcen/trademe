//
//  GetCategoriesRequest.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

protocol GetCategoriesRequestDelegate {
    func handleGetCategoriesSuccessWithObject<T>(response: SuccessResponse<T>)
    func handleGetCategoriesFailure()
}

class GetCategoriesRequest: NSObject, Requestable {
    func handleSuccessWithObject<TradeMeCategory>(response: SuccessResponse<TradeMeCategory>) {
        delegate.handleGetCategoriesSuccessWithObject(response: response)
    }
    
    func handleFailure(error: Error?) {
        delegate.handleGetCategoriesFailure()
    }
    
    func handleNetworkError(error: Error) {
        delegate.handleGetCategoriesFailure()
    }
    
    var method: HTTPMethod = .GET
    
    var urlPath: String = "/Categories/0.json"
    
    var pathParameters: [String : String]? = nil
    
    var queryParameters: [String : String]? = nil
    
    var requestBody: Codable? = nil
    
    let delegate: GetCategoriesRequestDelegate
    
    init(delegate: GetCategoriesRequestDelegate) {
        self.delegate = delegate
        super.init()
    }
}
