//
//  GetSearchItemsRequest.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

protocol GetSearchItemsRequestDelegate {
    func handleGetSearchItemsSuccessWithObject<T>(response: SuccessResponse<T>)
    func handleGetSearchItemsFailure()
}

class GetSearchItemsRequest: NSObject, Requestable {
    
    var method: HTTPMethod = .GET
    
    var urlPath: String = "/Search/General.json"
    
    var costumHeaders: [String: String]? { return Constants.apiAuthHeader }
    
    var pathParameters: [String : String]?
    
    var queryParameters: [String : String]? = [:]
    
    var requestBody: Codable?
    
    let delegate: GetSearchItemsRequestDelegate
    
    init(categoryNumber: String? = nil, searchString: String? = nil, delegate: GetSearchItemsRequestDelegate) {
        if let number = categoryNumber {
            queryParameters?["category"] = number
        }
        
        if let searchString = searchString {
            queryParameters?["search_string"] = searchString
        }
        
        self.delegate = delegate
        super.init()
    }
    
    func handleSuccessWithObject<T>(response: SuccessResponse<T>) {
        delegate.handleGetSearchItemsSuccessWithObject(response: response)
    }
    
    func handleFailure(error: Error?) {
        delegate.handleGetSearchItemsFailure()
    }
    
    func handleNetworkError(error: Error) {
        delegate.handleGetSearchItemsFailure()
    }
}
