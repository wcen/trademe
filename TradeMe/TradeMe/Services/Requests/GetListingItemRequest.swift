//
//  GetListingItemRequest.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

protocol GetListingItemRequestDelegate {
    func handleGetListingItemSuccessWithObject<T>(response: SuccessResponse<T>)
    func handleGetListingItemFailure()
}

class GetListingItemRequest: NSObject, Requestable {
    var method: HTTPMethod = .GET
    
    var urlPath: String = ""
    
    var costumHeaders: [String: String]? { return Constants.apiAuthHeader }
    
    var pathParameters: [String : String]?
    
    var queryParameters: [String : String]?
    
    var requestBody: Codable?
    
    let delegate: GetListingItemRequestDelegate
    
    init(listingId: Int, delegate: GetListingItemRequestDelegate) {
        urlPath = "/Listings/\(listingId).json"
        self.delegate = delegate
        super.init()
    }
    
    func handleSuccessWithObject<T>(response: SuccessResponse<T>) {
        delegate.handleGetListingItemSuccessWithObject(response: response)
    }
    
    func handleFailure(error: Error?) {
        delegate.handleGetListingItemFailure()
    }
    
    func handleNetworkError(error: Error) {
        delegate.handleGetListingItemFailure()
    }
}
