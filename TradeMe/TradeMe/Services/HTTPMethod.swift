//
//  HTTPMethod.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

public enum HTTPMethod: String {
    case GET
    case POST
}
