//
//  Constants.swift
//  TradeMe
//
//  Created by Weixiong Cen on 20/11/20.
//

import Foundation

struct Constants {
    static let apiVersion = "v1"
    static let baseURL = "https://api.tmsandbox.co.nz/\(apiVersion)"
    static let apiConsumerKey = "A1AC63F0332A131A78FAC304D007E7D1"
    static let apiConsumerSecret = "EC7F18B17A062962C6930A8AE88B16C7"
    static let apiAuthHeader = ["Authorization": "OAuth oauth_consumer_key=\"\(apiConsumerKey)\", oauth_signature_method=\"PLAINTEXT\", oauth_signature=\"\(apiConsumerSecret)&\""]
    
    static let maxListing = 20
}
